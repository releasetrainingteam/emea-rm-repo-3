<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Release_WFA_Send_Email_To_Release_Managers</fullName>
        <description>Release WFA Send Email To Release Managers</description>
        <protected>false</protected>
        <recipients>
            <recipient>cpardofabriguez@releasetraining.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tcreemers@emeareleasetraining.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>RM_Release_WFA_Uncheck_Ready_To_Deploy</fullName>
        <description>This WFA will make sure a newly created release is not ready for deployment upon creation.</description>
        <field>RM_Ready_To_Deploy__c</field>
        <literalValue>0</literalValue>
        <name>RM Release WFA Uncheck Ready To  Deploy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>RM_WF_Send_Email_Technical_Release</fullName>
        <actions>
            <name>Release_WFA_Send_Email_To_Release_Managers</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>RM_Release__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Techncal</value>
        </criteriaItems>
        <description>This WF send out an email when a technical release has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
