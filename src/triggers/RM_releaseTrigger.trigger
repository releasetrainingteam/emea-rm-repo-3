trigger RM_releaseTrigger on RM_Release__c (before insert,before update,after insert, after update) 
{
    if(Trigger.isAfter)
    {
        if(Trigger.isInsert)
        {
            RM_releaseManagement.updateReadyToRelease();
        }
    }
}