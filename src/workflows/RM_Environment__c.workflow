<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>RM_WFA_Email_Prod_Created</fullName>
        <description>This WFA will send an email to the record creator</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <recipient>sytruong@releasetraining.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/ContactFollowUpSAMPLE</template>
    </alerts>
    <rules>
        <fullName>RM_WF_Send_Email_Prod_Selected</fullName>
        <actions>
            <name>RM_WFA_Email_Prod_Created</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>RM_Environment__c.RM_Environment_Name__c</field>
            <operation>equals</operation>
            <value>PROD</value>
        </criteriaItems>
        <description>This WF will send an email when a Prod environment record has been created</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
