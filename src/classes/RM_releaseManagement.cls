public class RM_releaseManagement
{
    public static void updateReadyToRelease()
    {
        List<RM_Release__c> listRelease = [SELECT Id,RM_Project_Name__c,RM_Ready_To_Deploy__c  FROM RM_Release__c];
        
        for(RM_Release__c r: listRelease )
        {
            // Update all releases belonging to the EMEA project that have not ended yet
            if (r.RM_Project_Name__c == 'EMEA' && r.RM_Release_End_Date__c >= system.today())
            {
                r.RM_Ready_To_Deploy__c = true;
                update r;
            } 
            // Deactivate releases that are already overdue
            if (r.RM_Release_End_Date__c < system.today() && r.RM_Ready_To_Deploy__c) {
                r.RM_Ready_To_Deploy__c = false;
            }
        }
    }
}